<%-- 
    Document   : formulario Detalle recibo
    Created on : 18-oct-2014, 00:01:56
    Author     : miguel
--%>


<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" type="image/x-icon" href="../oc-img/warning-icon.png" />
        <link rel="stylesheet" type="text/css" href="oc-css/layout.css" />
        <title>Detalle Recibo</title>
    </head>
    <body>
    <center>
         <%@ include file = "menu-desplegable.jsp" %> 
          <br> <br> <br>
        <h1>Detalle Recibo</h1>
        
        <p>N&uacute;mero de recibo: ${recibo.idRecibos}</p>
        <p>Categor&iacute;a: ${recibo.categoria}</p>
        <p>Tipo Recibo: ${recibo.tipoPago}</p>
        <p>Estado: ${recibo.descripcionEstado}</p>
        <p>Situaci&oacute;n Pago: ${recibo.descripcionSituacionPago}</p>
        <p>Importe: ${recibo.importe} &euro;</p>
        <p>Fecha Aprobaci&ocaute;n: ${recibo.fechasAprobacion}</p>
        <p>Fecha de Cobro: ${recibo.fechaCobro}</p>
        <p>Fecha L&iacute;mite pago: ${recibo.fechaLimitePago}</p>
        
      <h3>
        
          
          <c:choose>
            <c:when test="${recibo.estado eq 'E'}"><a href="generarDocumentoPago.htm?id=${recibo.idRecibos}">Generar documento de pago</a></c:when>
            <c:otherwise> <a href="downloadReciboPDF.htm?id=${recibo.idRecibos}">Descargar en PDF</a> </c:otherwise>
          </c:choose>
      
      </h3>
    </center>
    </body>
</html>
