package org.oficinaciudadano.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.oficinaciudadano.beans.UsuarioBean;


import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class SessionInterceptor extends HandlerInterceptorAdapter {

    protected final Log logger = LogFactory.getLog(getClass());

    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception {


        Object obj = request.getSession().getAttribute("usuario");

        UsuarioBean usuario = (UsuarioBean) obj;
        logger.info("SessionInterceptor > Usuario actual: " + usuario);

        if (usuario == null
                && !request.getRequestURL().toString().contains("index")
                && !request.getRequestURL().toString().contains("logging")) {

            request.setAttribute("mensaje", "Para acceder a la página es necesario estar logueado :)");
            response.sendRedirect("index.htm");

            return false;
        } else if (request.getRequestURL().toString().contains("logout")) {
            
            request.getSession().removeAttribute("usuario");
            response.sendRedirect("index.htm");
            
            logger.info("SessionInterceptor > Logout hecho");
            return false;
        } else if (request.getRequestURL().toString().contains("bad_logging")){
            response.sendRedirect("index.htm");
            logger.info("SessionInterceptor > Login incorrecto - Volviendo a index.");
            return false;
        } else {
            return true;
        }

//            long startTime = System.currentTimeMillis();
//		logger.info("Request URL::" + request.getRequestURL().toString()
//				+ ":: Start Time=" + System.currentTimeMillis());
//		request.setAttribute("startTime", startTime);
//		return true;
    }

    @Override
    public void postHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {
//		System.out.println("Request URL::" + request.getRequestURL().toString()
//				+ " Sent to Handler :: Current Time=" + System.currentTimeMillis());
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
            HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
//        long startTime = (Long) request.getAttribute("startTime");
//		logger.info("Request URL::" + request.getRequestURL().toString()
//				+ ":: End Time=" + System.currentTimeMillis());
//		logger.info("Request URL::" + request.getRequestURL().toString()
//				+ ":: Time Taken=" + (System.currentTimeMillis() - startTime));
    }
}
