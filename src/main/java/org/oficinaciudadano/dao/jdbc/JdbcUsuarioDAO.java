/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oficinaciudadano.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.oficinaciudadano.beans.CambioDatosBean;
import org.oficinaciudadano.beans.CiudadanoBean;
import org.oficinaciudadano.beans.DireccionBean;
import org.oficinaciudadano.beans.UsuarioBean;
import org.oficinaciudadano.dao.UsuarioDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcUsuarioDAO implements UsuarioDAO {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        //NamedParameterJdbcTemplate(dataSource);
    }

    //no need to set datasource here
    public void insert(UsuarioBean usuario) {
//
//        String sql = "INSERT INTO USUARIOS "
//                + "(CUST_ID, NAME, AGE) VALUES (?, ?, ?)";
//
//        getJdbcTemplate().update(sql, new Object[]{usuario.getNombre(),
//                    usuario.getPassword(), usuario.getTipoUsuario()
//                });

    }

    @Override
    public void login(UsuarioBean usuario) {


        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean existeUsuario(UsuarioBean usuario) {
        String sql = "select count(*) from Usuarios where NombreUsuario = :nombre and Password = :password";

        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(usuario);

        return this.namedParameterJdbcTemplate.queryForObject(sql, namedParameters, Integer.class) > 0;
    }

     public String tipoDeUsuario(UsuarioBean usuario) {
        String sql = "select TipoUsuario from Usuarios where NombreUsuario = :nombre and Password = :password";

        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(usuario);

        return this.namedParameterJdbcTemplate.queryForObject(sql, namedParameters,String.class);
    }
     
    public String dniUsuario(UsuarioBean usuario){
        String sql = "select Ciudadanos_DNI from Usuarios where NombreUsuario = :nombre and Password = :password";

        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(usuario);

        return this.namedParameterJdbcTemplate.queryForObject(sql, namedParameters,String.class);
    }
    
    public DireccionBean obtenerDireccion(UsuarioBean usuario){
        String nombreCalle = "select nombreCalle from Direccion, Ciudadanos where Ciudadanos.DNI  = '" + usuario.getDni() + "' and Direccion.idDireccion = Ciudadanos.Direccion_idDireccion";
        String numeroPortal = "select NroPortal from Direccion, Ciudadanos where Ciudadanos.DNI  = '" + usuario.getDni() + "' and Direccion.idDireccion = Ciudadanos.Direccion_idDireccion";
        String codigoPostal = "select CodigoPostal from Direccion, Ciudadanos where Ciudadanos.DNI  = '" + usuario.getDni() + "' and Direccion.idDireccion = Ciudadanos.Direccion_idDireccion";
        String id = "select idDireccion from Direccion, Ciudadanos where Ciudadanos.DNI  = '" + usuario.getDni() + "' and Direccion.idDireccion = Ciudadanos.Direccion_idDireccion";
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(usuario);
        DireccionBean direccion = new DireccionBean();
        direccion.setCodigoPostal(this.namedParameterJdbcTemplate.queryForObject(codigoPostal, namedParameters,String.class));
        direccion.setNombreCalle(this.namedParameterJdbcTemplate.queryForObject(nombreCalle, namedParameters,String.class));
        direccion.setNumeroPortal(this.namedParameterJdbcTemplate.queryForObject(numeroPortal, namedParameters,String.class));
        direccion.setIdDireccion(this.namedParameterJdbcTemplate.queryForObject(id, namedParameters,String.class));
        return direccion;
    }
     
    @Override
    public boolean insertarCiudadanoPadron(CiudadanoBean ciudadano) {
        
              String checkDireccion="SELECT COUNT(idDireccion) FROM Direccion";
               SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(ciudadano);
        Integer numDireccion=(Integer) this.namedParameterJdbcTemplate.queryForObject(checkDireccion, namedParameters, Integer.class);
        numDireccion=numDireccion+1;
       //String insertDireccion="INSERT INTO Direccion (idDireccion,NombreCalle) VALUES ("+numDireccion+",'"+ciudadano.getDireccion()+"')";
        String insertDireccion="INSERT INTO Direccion (idDireccion,NombreCalle) VALUES (:numeroDireccion,:direccionCiudadano)";
       Map namedParameters2 = new HashMap();
       namedParameters2.put("numeroDireccion", numDireccion);
       namedParameters2.put("direccionCiudadano", ciudadano.getDireccion());
       int s = this.namedParameterJdbcTemplate.update(insertDireccion, namedParameters2);//update(insertDireccion,SqlParameterSource.class);
   
       
        Map namedParametersCiudadano = new HashMap();
       namedParametersCiudadano.put("dni", ciudadano.getDni());
       namedParametersCiudadano.put("nombre", ciudadano.getNombre());       
       namedParametersCiudadano.put("apellido1", ciudadano.getApellido1());       
       namedParametersCiudadano.put("apellido2", ciudadano.getApellido2());       
       namedParametersCiudadano.put("fechaNacimiento", ciudadano.getFechaNacimiento());       
       namedParametersCiudadano.put("lugarNacimiento", ciudadano.getLugarNacimiento());       
       namedParametersCiudadano.put("nivelInstruccion", ciudadano.getNivelInstruccion());       
       namedParametersCiudadano.put("tipoVivienda", ciudadano.getTipoVivienda());   
       namedParametersCiudadano.put("email", ciudadano.getEmail());   
       namedParametersCiudadano.put("numDireccion", numDireccion);   

       
       String sqlCiudadano="INSERT INTO Ciudadanos(DNI,Nombre,Apellido1,Apellido2,FechaNacimiento,LugarNacimiento,NivelInstruccion,TipoVivienda,Email,Direccion_idDireccion)"
                + " VALUES (:dni,:nombre,:apellido1,:apellido2,:fechaNacimiento,:lugarNacimiento,:nivelInstruccion,:tipoVivienda,:email,:numDireccion);";
     
       int fin = this.namedParameterJdbcTemplate.update(sqlCiudadano, namedParametersCiudadano);//update(insertDireccion,SqlParameterSource.class);
       /*    
        String sqlCiudadano="INSERT INTO Ciudadanos(DNI,Nombre,Apellido1,Apellido2,FechaNacimiento,LugarNacimiento,NivelInstruccion,TipoVivienda,Email,Direccion_idDireccion)"
                + " VALUES ('"+ciudadano.getDni()+"','"+
                ciudadano.getNombre()+"','"+ciudadano.getApellido1()+"','"+ciudadano.getApellido2()+
                "','"+ciudadano.getFechaNacimiento()+"','"+ciudadano.getLugarNacimiento()+
                "','"+ciudadano.getNivelInstruccion()+"','"+ciudadano.getTipoVivienda()+"','"+ciudadano.getEmail()+
                "','"+numDireccion+"');";
        
         s = this.namedParameterJdbcTemplate.execute(sqlCiudadano,null);
*/
         return true;
    }
    
    public void insertarSolicitudCambioDomicilio(UsuarioBean usuario, DireccionBean direccion){
        //Obtener id de dirección actual
        DireccionBean direccionActual = obtenerDireccion(usuario);
        //Generar string con campos de nueva dirección
        String direccionNueva = direccion.getNombreCalle() + "," + direccion.getNumeroPortal() + "," + direccion.getCodigoPostal();
        //Obtener número de elementos en tabla para definir índice
        String checkId = "select count(*) FROM CambioDatos";
        Map checkCountId = new HashMap();
        Integer numId = this.namedParameterJdbcTemplate.queryForObject(checkId, checkCountId, Integer.class);        
        Map namedParameters = new HashMap();
        namedParameters.put("Direccion_idDireccion", direccionActual.getIdDireccion());
        namedParameters.put("Ciudadanos_DNI", usuario.getDni());
        namedParameters.put("NewDireccion", direccionNueva);
        namedParameters.put("idCambioDatos", numId+1);
        namedParameters.put("FechaSolicitud", null);
        String sql = "insert into CambioDatos values(:idCambioDatos,:Ciudadanos_DNI,:Direccion_idDireccion,:NewDireccion,:FechaSolicitud)";
        this.namedParameterJdbcTemplate.update(sql, namedParameters);
    }
    
    public List<CambioDatosBean> obtenerSolicitudesCambioDomicilio(){
        List<CambioDatosBean> solicitudes;
        String sql = "select * from CambioDatos";
        
    /*    return this.namedParameterJdbcTemplate.query(sql, 
        new RowMapper(){

            @Override
            public Object mapRow(ResultSet rs, int i) throws SQLException {
                CambioDatosBean solicitud = new CambioDatosBean();
                int a = rs.getInt("idCambioDatos");
                solicitud.setDni(rs.getString("Ciudadanos_DNI"));
                solicitud.setIdDireccion(rs.getInt("Direccion_idDireccion"));
                solicitud.setFechaSolicitud(rs.getString("FechaSolicitud"));
                solicitud.setNuevaDireccion(rs.getString("NewDireccion"));
                return solicitud;
            }
            
        }); */
        return null;
    }
}
