<%-- 
    Document   : registro-ciudadano
    Created on : 10-oct-2014, 19:27:38
    Author     : adrian
--%>

<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" type="image/x-icon" href="oc-img/warning-icon.png" />
        <link rel="stylesheet" type="text/css" href="oc-css/layout.css" />
        <script type="text/javascript" src="menu-desplegable.js"></script>
        
        <title>Registro de ciudadanos</title>
    </head>
    
    <body>
        <jsp:useBean id="ciudadano" class="org.oficinaciudadano.beans.CiudadanoBean" scope="session"/>
    <center>
        <%@ include file = "menu-desplegable.jsp" %>
        <h1>Registrarse como usuario</h1>
        <form action="insertarEnPadron.htm" method="POST" commandName="ciudadano" modelAttribute="ciudadano">
            <h1>Datos del ciudadano</h1>
            Nombre: <input type="text" name="nombre"/><br>
            Apellido 1: <input type="text" name="apellido1"/><br>
            Apellido 2: <input type="text" name="apellido2"/><br>
            DNI <input type="text" name="dni"/><br>
            Fecha de nacimiento : <input type="text" name="fechaNacimiento"/><br>
            Lugar de nacimiento : <input type="text" name="lugarNacimiento"/><br>
            Nivel de instruccion : <input type="text" name="nivelInstruccion"/><br>
            Tipo de vivienda : <input type="text" name="tipoVivienda"/><br>
            Correo electrónico : <input type="text" name="email"/><br>
            Dirección actual : <input type="text" name="direccion"/><br>
            <input type="submit" value="submit" />
        </form>
    </center>
</body>
</html>
