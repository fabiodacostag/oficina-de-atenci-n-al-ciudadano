<%-- 
    Document   : listadoRecibos
    Created on : Nov 14, 2014, 2:10:51 PM
    Author     : David
--%>

<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" type="image/x-icon" href="oc-img/warning-icon.png" />
        <link rel="stylesheet" type="text/css" href="oc-css/layout.css" />
        <title>Listado de Recibos</title>
    </head>
    <body>
        <jsp:useBean id="usuario" class="org.oficinaciudadano.beans.UsuarioBean" scope="session"/>
    <center>
        <%@ include file = "menu-desplegable.jsp" %>
        <br> <br> <br>
        <h1>Listado de Recibos</h1>
        <br> <br> <br>

        <table border="1">
            <th>ID de Recibo</th>
            <th>Situacion de Pago</th>
            <th>Fecha de Aprobacion</th>
            <th>Fecha de Cobro</th>
            <th>Fecha Limite de Pago</th>
                <c:forEach items="${resultado}" var="recibo">
                <tr>
                    <td><a href="detalleRecibo.htm?id=${recibo.idRecibos}">${recibo.idRecibos}</a></td>
                    <td>${recibo.situacionPago}</td>
                    <td>${recibo.fechasAprobacion}</td>
                    <td>${recibo.cobro}</td>
                    <td>${recibo.fechaLimitePago}</td>
                </tr>
            </c:forEach>
        </table>
        <br><br>
        <h3><a href="">Descargar en PDF</a></h3>
    </center>
</body>
</html>
