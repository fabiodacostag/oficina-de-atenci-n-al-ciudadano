/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oficinaciudadano.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.oficinaciudadano.beans.DireccionBean;
import org.oficinaciudadano.beans.ReciboBean;
import org.oficinaciudadano.beans.UsuarioBean;
import org.oficinaciudadano.dao.jdbc.JdbcUsuarioDAO;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author Miguel
 */
public class DocumentoPagoReciboPDFBuilder extends AbstractITextPdfView {

    JdbcUsuarioDAO dao;

    @Override
    protected void buildPdfDocument(Map model, Document doc, PdfWriter writer, HttpServletRequest request, HttpServletResponse response) throws Exception {

        // get data model which is passed by the Spring container
        ReciboBean recibo = (ReciboBean) model.get("recibo");

        UsuarioBean u = (UsuarioBean) request.getSession().getAttribute("usuario");
        System.out.println(u);


        ApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(this.getServletContext());

        dao = (JdbcUsuarioDAO) context.getBean("jdbcUsuarioDAO");

        DireccionBean d = dao.obtenerDireccion(u);

        doc.add(new Paragraph("Documento de pago de recibo"));

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100.0f);
        table.setWidths(new float[]{4.0f, 6.0f});
        table.setSpacingBefore(10);

        // define font for table header row

        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(BaseColor.WHITE);

        // define table header cell
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(BaseColor.GRAY);
        cell.setPadding(4);

        // write table header
        cell.setPhrase(new Phrase("Número de recibo:", font));
        table.addCell(cell);
        table.addCell(String.valueOf(recibo.getIdRecibos()));

        cell.setPhrase(new Phrase("Nombre ", font));
        table.addCell(cell);
        table.addCell(u.getNombre());

        cell.setPhrase(new Phrase("DNI: ", font));
        table.addCell(cell);
        table.addCell(u.getDni());
        // table.addCell("");

        cell.setPhrase(new Phrase("Direccion: ", font));
        table.addCell(cell);
        table.addCell(d.getNombreCalle() + " " + d.getNumeroPortal() + " " + d.getCodigoPostal());

        cell.setPhrase(new Phrase("Categoría", font));
        table.addCell(cell);
        table.addCell(recibo.getCategoria());
        // table.addCell("");


        cell.setPhrase(new Phrase("Tipo Recibo:", font));
        table.addCell(cell);
        table.addCell(recibo.getTipoPago());
        //table.addCell("");

        cell.setPhrase(new Phrase("Importe", font));
        table.addCell(cell);
        table.addCell(recibo.getImporte() + " €");
        //table.addCell("");

        cell.setPhrase(new Phrase("Cuenta bancaria ingreso", font));
        table.addCell(cell);
        table.addCell("1234-33-1234-1239863452");

        cell.setPhrase(new Phrase("Fecha límite de pago", font));
        table.addCell(cell);
        table.addCell(recibo.getFechaLimitePago().toString());
        // table.addCell("");
        // write table row data


        doc.add(new LineSeparator());
        doc.add(table);
    }
}
