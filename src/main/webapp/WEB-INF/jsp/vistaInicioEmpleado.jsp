<%-- 
    Document   : vistaInicioEmpleado
    Created on : 15-nov-2014, 14:25:21
    Author     : german
--%>
<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" type="image/x-icon" href="../oc-img/warning-icon.png" />
        <link rel="stylesheet" type="text/css" href="oc-css/layout.css" />
        <title>Pantalla de empleado</title>
    </head>
    <body>
    <center>
        <%@ include file = "menu-desplegable.jsp" %> 
        <br> <br> <br>        
        <h1>Bienvenido ${sessionScope.usuario.nombre}</h1>
        <br>
        <h2> Visualizar solicitudes de cambio de domicilio</h2>
        
        <jsp:useBean id="listaSolicitudesBean" class="org.oficinaciudadano.beans.ListaSolicitudesBean" scope="page" />
        
        <table border=1 ALIGN=CENTER>
            <th>Dni</th>
            <th>Direcci�n actual</th>
            <th>Nueva direcci�n</th>
            <th>Fecha solicitud</th>     
            <th>Validar</th>
            <th>Denegar</th>
            <c:forEach var="row" items="${listaSolicitudesBean.listaSolicitudesCambioDomicilio}" begin="0">
                <form:form method="POST" action="validarSolicitud.htm" commandName="ciudadano" modelAttribute="ciudadano">
                <tr>
                    <td>${row.dni}</td>         
                    <td>${row.idDireccion}</td>
                    <td>${row.nuevaDireccion}</td>
                    <td>${row.fechaSolicitud}</td>
                    <td>
                        <input type="submit" name="validar" value="Validar solicitud" />
                    </td>
                    <td>
                        <input type="submit" name="denegar" value="Denegar solicitud" />
                    </td>
                </tr>    
                </form:form>
           </c:forEach>            
        </table>
        
        <br>
        <h2> Registrar a un ciudadano en el padr�n</h2>   
        <form:form method="GET" action="formularioRegistroPadron.htm" commandName="usuario" modelAttribute="usuario">
            <input type="submit" value="Registrar"/>
        </form:form>
        <br>
        <h2> Cerrar sesi�n</h2>               
        <form:form method="POST" action="logout.htm"  commandName="usuario" modelAttribute="usuario">
            <input type="submit" name="cerrar_sesion" value="Cerrar sesi�n" /> 
        </form:form>
    </center>
    </body>
</html>
