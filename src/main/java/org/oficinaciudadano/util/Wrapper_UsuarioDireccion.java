/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oficinaciudadano.util;

import org.oficinaciudadano.beans.DireccionBean;
import org.oficinaciudadano.beans.UsuarioBean;

/**
 *
 * @author adrian
 */
public class Wrapper_UsuarioDireccion {
    
    private DireccionBean direccion;
    private UsuarioBean usuario;

    public DireccionBean getDireccion() {
        return direccion;
    }

    public void setDireccion(DireccionBean direccion) {
        this.direccion = direccion;
    }

    public UsuarioBean getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioBean usuario) {
        this.usuario = usuario;
    }
}
