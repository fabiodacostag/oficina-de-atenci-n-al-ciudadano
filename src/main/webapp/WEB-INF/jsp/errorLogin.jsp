<%-- 
    Document   : error
    Created on : 11-oct-2014, 15:28:49
    Author     : adrian
--%>

<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" type="image/x-icon" href="oc-img/warning-icon.png" />
        <link rel="stylesheet" type="text/css" href="oc-css/layout.css" />
        <title>Error de login</title>
    </head>
    <body>
    <center>
        <h1>Error de login</h1>
        <c:choose>
            <c:when test="${sessionScope.usuario==null}">
                <form:form method="POST" action="bad_logging.htm"  commandName="usuario" modelAttribute="usuario">
                    <input type="submit" name="return_login" value="Volver a la p�gina de Inicio" /> 
                </form:form>
            </c:when>
        </c:choose>
    </center>
    </body>
</html>
