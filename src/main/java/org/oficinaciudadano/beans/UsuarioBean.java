/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oficinaciudadano.beans;

import java.io.Serializable;


/**
 *
 * @author german
 */
public class UsuarioBean implements Serializable{

    private String nombre;
    private String password;
    private String tipoUsuario;
    private String dni;
    
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    public UsuarioBean() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Override
    public String toString() {
        return "UsuarioBean{" + "dni=" + dni + ", nombre=" + nombre + ", password=" + password + ", tipoUsuario=" + tipoUsuario + '}';
    }
    
    
    
}
