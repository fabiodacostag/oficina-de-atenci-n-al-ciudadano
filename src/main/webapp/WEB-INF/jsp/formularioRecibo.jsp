<%-- 
    Document   : listadoRecibos
    Created on : Nov 13, 2014, 7:14:21 PM
    Author     : David
--%>

<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" type="image/x-icon" href="oc-img/warning-icon.png" />
        <link rel="stylesheet" type="text/css" href="oc-css/layout.css" />
        <title>Formulario Detalle Recibo</title>
    </head>
    <body>  
        <jsp:useBean id="usuario" class="org.oficinaciudadano.beans.UsuarioBean" scope="session"/>
    <center>
        <%@ include file = "menu-desplegable.jsp" %> 
        <br> <br> <br>
        <h1>Solicitud de listado de recibos</h1>
        <br>
        <h3>Pulse el bot�n Listar para obtener el listado de recibos</h3>
        <form:form method="POST" action="solicitarListadoRecibos.htm" modelAttribute="ciudadano">
            DNI: <input type="text" name="dni" value="${sessionScope.usuario.dni}" readonly />
            <input type="submit" value="Listar" /> 
        </form:form>
    </center>
</body>
</html>
