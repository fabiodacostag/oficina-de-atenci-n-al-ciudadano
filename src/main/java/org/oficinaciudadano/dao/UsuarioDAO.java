/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oficinaciudadano.dao;

import org.oficinaciudadano.beans.CiudadanoBean;
import org.oficinaciudadano.beans.UsuarioBean;

/**
 *
 * @author Miguel
 */
public interface UsuarioDAO {

    public void insert(UsuarioBean usuario);

    public void login(UsuarioBean usuario);

    public boolean existeUsuario(UsuarioBean usuario);

    public boolean insertarCiudadanoPadron(CiudadanoBean ciudadano);
}
