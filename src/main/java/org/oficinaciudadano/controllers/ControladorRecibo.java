/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oficinaciudadano.controllers;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.oficinaciudadano.beans.CiudadanoBean;
import org.oficinaciudadano.beans.ReciboBean;
import org.oficinaciudadano.beans.UsuarioBean;
import org.oficinaciudadano.dao.jdbc.JdbcReciboDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Miguel
 */
@Controller
public class ControladorRecibo {

    @Autowired
    JdbcReciboDAO reciboDAo;
    protected final Log logger = LogFactory.getLog(getClass());

    @RequestMapping(value = "/detalleRecibo", method = RequestMethod.GET)
    public String detalleRecibo(@RequestParam int id, ModelMap model) {
        try {
            ReciboBean recibo = reciboDAo.obtenerRecibo(id);
            logger.info("ControladorRecibo > RECIBO:  " + recibo);

            model.addAttribute("recibo", recibo);
            return "detalleRecibo";
        } catch (Exception ex) {
            model.addAttribute("mensaje", ex.getMessage());
            return "error";
        }
    }

    @RequestMapping(value = "/crearRecibo", method = RequestMethod.POST)
    public String crearRecibo(@ModelAttribute("SpringWeb") ReciboBean recibo,
            ModelMap model) {

        reciboDAo.crearRecibo(recibo);
        model.addAttribute("recibo", recibo);

        return "detalleRecibo";
    }

    @RequestMapping(value = "/downloadReciboPDF", method = RequestMethod.GET)
    public ModelAndView downloadPDF(@RequestParam int id) {
        // create some sample data

        try {
            ReciboBean recibo = reciboDAo.obtenerRecibo(id);
            logger.info("ControladorRecibo > RECIBO para pdf:  " + recibo);

            return new ModelAndView("pdfView", "recibo", recibo);
        } catch (Exception ex) {

            return new ModelAndView("error", "mensaje", ex.getMessage());
        }
        // return a view which will be resolved by an excel view resolver

    }
    
    @RequestMapping(value = "/generarDocumentoPago", method = RequestMethod.GET)
    public ModelAndView generarDocumentoPago(@RequestParam int id) {
        // create some sample data

        try {
            ReciboBean recibo = reciboDAo.obtenerRecibo(id);
            logger.info("ControladorRecibo > DocumentoPago RECIBO :  " + recibo);

            return new ModelAndView("documentoPago", "recibo", recibo);
        } catch (Exception ex) {

            return new ModelAndView("error", "mensaje", ex.getMessage());
        }
        // return a view which will be resolved by an excel view resolver

    }
    
    @RequestMapping(value = "/solicitarListadoRecibos", method = RequestMethod.POST)
    public String listarRecibos3(ModelMap model, @ModelAttribute("SpringWeb") CiudadanoBean ciudadano) {
        
        logger.info("ControladorRecibo > Solicitando listado de recibos del usuario con DNI: " + ciudadano.getDni());
        
        try {
            String dni = ciudadano.getDni();
            List<ReciboBean> resultado = reciboDAo.listarRecibos(dni);
            logger.info("ControladorRecibo > Listado de recibos realizado.");
            model.addAttribute("resultado", resultado);
        } catch (Exception e) {
            logger.info("Excepcion: " + e);
        }
        
        return "listadoRecibos";
    }
}
