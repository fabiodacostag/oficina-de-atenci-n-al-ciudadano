<%-- 
    Document   : menu-desplegable
    Created on : 12-oct-2014, 10:43:46
    Author     : adrian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h1> OFICINA DE ANTENCI�N AL CIUDADANO DE ANTAS DE ULLA </h1>
<ul id ="menuDesp">
<li><a href="<c:url value="index.htm"/>">Inicio</a>
    <ul>  
        <li><a href="#">Conoce Antas de Ulla</a></li>
        <li><a href="#">Mapa del Sitio</a></li>
    </ul>
</li>
<li><a href="#">Padr�n Municipal de Habitantes</a>
    <ul>
        <li><a href="<c:url value="formularioRegistroPadron.htm"/>">Alta en el padr�n</a></li>
        <li><a href="<c:url value="solicitarCambioDomicilio.htm"/>">Cambio de domicilio</a></li>
        <li><a href="#">Certificado empadronamiento</a></li>
    </ul>
</li>
<li><a href="#">Atenci�n al Contribuyente</a>
  <ul>
        <li><a href="<c:url value="formularioRecibo.htm"/>">Consulta de recibos</a></li>
        <li><a href="#" target="_new">Impresi�n de documentos</a></li>
        <li><a href="#" target="_new">Domiciliaci�n de tributos</a></li>
        <li><a href="#" target="_new">Pago de recibos</a></li>
        <li><a href="#" target="_new">Certificado corriente de pago</a></li>
    </ul>
</li>
<li><a href="#">Gestiones de Tr�fico</a>
  <ul>
        <li><a href="#" target="_new">Aparcamiento para residentes</a></li>
        <li><a href="#" target="_new">Estacionamiento para discapacitados</a></li>
    </ul>
</li>
<li><a href="#">Servicios Generales</a>
  <ul>
        <li><a href="#" target="_new">Instancia general</a></li>
        <li><a href="#" target="_new">Sugerencias y quejas</a></li>
    </ul>
</li>
<li><a href="#">Usuario</a>
  <ul>
        <li><a href="#" target="_new">Datos de contacto</a></li>
        <li><a href="#" target="_new">Buz�n de mensajes</a></li>
    </ul>
</li>
</ul>
