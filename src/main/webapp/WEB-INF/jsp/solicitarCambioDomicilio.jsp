<%-- 
    Document   : solicitarCambioDomicilio
    Created on : 22-nov-2014, 10:12:55
    Author     : adrian
--%>

<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" type="image/x-icon" href="oc-img/warning-icon.png" />
        <link rel="stylesheet" type="text/css" href="oc-css/layout.css" />
        <title>Solicitar cambio de domicilio</title>
    </head>
    <body>
        <jsp:useBean id="usuario" class="org.oficinaciudadano.beans.UsuarioBean" scope="session"/>
        <jsp:useBean id="direccion" class="org.oficinaciudadano.beans.DireccionBean" scope="session" />
    <center>
        <%@ include file = "menu-desplegable.jsp" %> 
        <br> <br> <br> 
        <h1>Solicitud de cambio de domicilio</h1>
        <br>
        <h2> <u>DNI del ciudadano:</u> ${sessionScope.usuario.dni}</h2> 
        <h2> <u>Direcci�n actual del ciudadano</u></h2>
        <h3> <u>Calle:</u> ${sessionScope.direccion.nombreCalle}  <u>Portal:</u> ${sessionScope.direccion.numeroPortal}  <u>C�digo Postal:</u> ${sessionScope.direccion.codigoPostal}</h3>  
        <br><br>
        <h2> <u>Nueva direcci�n del ciudadano</u></h2>
        <form:form method="POST" action="insertarSolicitudCambioDomicilio.htm" commandName="Wrapper_UsuarioDireccion" modelAttribute="Wrapper_UsuarioDireccion">
            <b><form:label path="direccion.nombreCalle">Calle:</form:label></b>
                    <form:input path="direccion.nombreCalle" />
            <b><form:label path="direccion.numeroPortal">Portal:</form:label></b>
                    <form:input path="direccion.numeroPortal" />   <br>
            <b><form:label path="direccion.codigoPostal">C�digo Postal:</form:label></b>
                    <form:input path="direccion.codigoPostal" />
            <b><form:label path="usuario.dni">DNI:</form:label></b>
                    <form:input path="usuario.dni" /><br>
            <input type="submit" name="cambioDomicilio" value="Solicitar" /> 
        </form:form>
    </center>
    </body>
</html>
