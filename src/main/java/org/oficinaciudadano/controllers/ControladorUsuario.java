/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oficinaciudadano.controllers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.oficinaciudadano.beans.DireccionBean;
import org.oficinaciudadano.beans.UsuarioBean;
import org.oficinaciudadano.dao.jdbc.JdbcUsuarioDAO;
import org.oficinaciudadano.util.Wrapper_UsuarioDireccion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;


@Controller
@SessionAttributes({"usuario","ciudadano","direccion"})
public class ControladorUsuario {

    @Autowired
    JdbcUsuarioDAO dao;
    
    
    protected final Log logger = LogFactory.getLog(getClass());

    @RequestMapping(value = "/logging", method = RequestMethod.POST)
    public String logging(@ModelAttribute("SpringWeb") UsuarioBean usuario,
            DireccionBean direccion, ModelMap model) {
        
        logger.info("ControladorUsuario > Logueándose el usuario " + usuario.getNombre());
        
       
        if (dao.existeUsuario(usuario)) {       
            usuario.setTipoUsuario(dao.tipoDeUsuario(usuario));
            usuario.setDni(dao.dniUsuario(usuario));
            direccion = dao.obtenerDireccion(usuario);
            model.addAttribute("usuario", usuario);
            model.addAttribute("direccion", direccion);
            logger.info("ControladorUsuario > El usuario ha iniciado sesion.");
            
            if (usuario.getTipoUsuario().matches("Empleado")){
                return "vistaInicioEmpleado";
            }
            
            return "index";//nombre de la jsp
            
        } else {           
            logger.info("ControladorUsuario > Login incorrecto.");
            return "errorLogin";
        }
    }
    
    @RequestMapping(value = "/bad_logging", method = RequestMethod.POST)
    public String bad_logging(@ModelAttribute("SpringWeb") UsuarioBean usuario,
            ModelMap model){
        
        return "index";
    }    
    
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String logout(@ModelAttribute("SpringWeb") UsuarioBean usuario,
            ModelMap model){
        
        return "index";
    }    
    
    @RequestMapping(value = "/insertarSolicitudCambioDomicilio",method = RequestMethod.POST)
    public String insertarSolicitudCambioDomicilio(ModelMap model, @ModelAttribute("SpringWeb")
        Wrapper_UsuarioDireccion wrapper){
        logger.info("ControladorUsuario > Insertando nueva solicitud de cambio de domicilio de " + wrapper.getUsuario().getNombre());
        logger.info("ControladorUsuario > DNI: " + wrapper.getUsuario().getDni() + " Calle: " + wrapper.getDireccion().getNombreCalle());
        try{
            dao.insertarSolicitudCambioDomicilio(wrapper.getUsuario(),wrapper.getDireccion());
            return "success";
        }
        catch(Exception e){
            logger.info("Error en ControladorEmpleado > Excepción insertando cambio domicilio: " + e); 
            return "index";
        }
    }      
}