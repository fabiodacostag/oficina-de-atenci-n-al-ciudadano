<%-- 
    Document   : success
    Created on : 23-nov-2014, 13:05:06
    Author     : adrian
--%>

<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" type="image/x-icon" href="oc-img/warning-icon.png" />
        <link rel="stylesheet" type="text/css" href="oc-css/layout.css" />
        <title>Solicitud realizada</title>
    </head>
    <body>
    <center>
        <%@ include file = "menu-desplegable.jsp" %> 
        <br> <br> <br>         
        <h1>Su solicitud ha sido realizada con �xito</h1>
    </center>
    </body>
</html>
