/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oficinaciudadano.controllers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.oficinaciudadano.beans.CiudadanoBean;
import org.oficinaciudadano.beans.DireccionBean;
import org.oficinaciudadano.beans.UsuarioBean;
import org.oficinaciudadano.dao.jdbc.JdbcReciboDAO;
import org.oficinaciudadano.util.Wrapper_UsuarioDireccion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ControladorIndex {

    @Autowired
    JdbcReciboDAO reciboDAo;
    protected final Log logger = LogFactory.getLog(getClass());

    @RequestMapping(value = "/index",method = RequestMethod.GET)
    public String logging(ModelMap model) {
        logger.info("ControladorIndex > Accediendo visitante.");
        model.addAttribute("usuario", new UsuarioBean());
        return "index";
    }  
    
    @RequestMapping(value = "/formularioRegistroPadron",method = RequestMethod.GET)
    public String registroPadron(ModelMap model) {
        logger.info("ControladorIndex > Registro PAdronoon Accediendo visitante.");
        return "formularioRegistroPadron";
    }  
    
    @RequestMapping(value = "/solicitarCambioDomicilio",method = RequestMethod.GET)
    public String cambioDomicilio(ModelMap model) {
        logger.info("ControladorIndex > Accediendo a solicitud de cambio de domicilio.");
        model.addAttribute("Wrapper_UsuarioDireccion", new Wrapper_UsuarioDireccion());
        return "solicitarCambioDomicilio";
    }
    
    @RequestMapping(value = "/formularioRecibo",method = RequestMethod.GET)
    public String formularioRecibo(ModelMap model) {
        logger.info("ControladorIndex > Accediendo a formulario recibo.");
        return "formularioRecibo";
    }
}