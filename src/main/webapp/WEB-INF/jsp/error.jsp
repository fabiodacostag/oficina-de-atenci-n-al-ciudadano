<%-- 
    Document   : error
    Created on : 11-oct-2014, 15:28:49
    Author     : adrian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<!DOCTYPE html>
<html>
     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" type="image/x-icon" href="oc-img/warning-icon.png" />
        <link rel="stylesheet" type="text/css" href="oc-css/layout.css" />
        <script type="text/javascript" src="menu-desplegable.js"></script>
        <title>404 Yes Found</title>
    </head>

    <body>       
    <center>
        <%@ include file = "menu-desplegable.jsp" %> 
        <h1>Error</h1>
        <h2>${mensaje}</h2>
    </center>
    </body>
</html>
