///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package servlets;
//
//
//import DAO.DAO;
//import beans.UsuarioBean;
//import java.io.IOException;
//import java.io.PrintWriter;
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
///**
// *
// * @author german
// */
//public class ControladorUsuarios extends HttpServlet {
//
//    /**
//     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
//     * methods.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//         
//         
//        HttpSession session = request.getSession(false);
//        UsuarioBean usuario = (UsuarioBean)session.getAttribute("Usuario");
//        
//        if(usuario==null){
//          usuario = new UsuarioBean();
//          session.setAttribute("Usuario",usuario);
//        }
//        
//      synchronized(session){
//
//        if(request.getParameter("login")!=null){
//            
//            DAO dao = new DAO();
//            
//            UsuarioBean usuarioRecuperado = dao.login(request.getParameter("nombreUsuario"), request.getParameter("password"));
//            
//            if (usuarioRecuperado==null){
//                gotoPage("/errorLogin.jsp",request,response);
//            }
//            else if (usuarioRecuperado.getTipoUsuario().matches("empleado")){
//                gotoPage("/vistaEmpleado.jsp",request,response);
//            }
//            else if (usuarioRecuperado.getTipoUsuario().matches("ciudadano")){
//                gotoPage("/vistaCiudadano.jsp",request,response);
//            }
//            else{
//                gotoPage("/login.jsp",request,response);
//            }                   
//
//        }
//      }
//        
//
//    }
//
//    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
//    /**
//     * Handles the HTTP <code>GET</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Handles the HTTP <code>POST</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Returns a short description of the servlet.
//     *
//     * @return a String containing servlet description
//     */
//    @Override
//    public String getServletInfo() {
//        return "Short description";
//    }// </editor-fold>
//
//     private void gotoPage(String address,HttpServletRequest request,HttpServletResponse response)
//          throws ServletException,IOException{
//        RequestDispatcher dispatcher= getServletContext().getRequestDispatcher(address);
//        dispatcher.forward(request,response);
//    }
//}
