<%-- 
    Document   : registroPadronVerificar
    Created on : 26-oct-2014, 13:36:37
    Author     : german
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" type="image/x-icon" href="../oc-img/warning-icon.png" />
        <link rel="stylesheet" type="text/css" href="oc-css/layout.css" />
        <title>Registro en padrón</title>
    </head>
    <jsp:useBean id="ciudadano" class="org.oficinaciudadano.beans.CiudadanoBean" scope="session"/>

    <center>
                <%@ include file = "menu-desplegable.jsp" %> 

        <h1>Se ha insertado correctamente</h1>
  
        <jsp:setProperty name="ciudadano" property="nombre" param="nombre"/><br> 
        <jsp:setProperty name="ciudadano" property="apellido1" param="apellido1"/><br> 
        <jsp:setProperty name="ciudadano" property="apellido2" param="apellido2"/><br> 
        <jsp:setProperty name="ciudadano" property="dni" param="dni"/>
        <jsp:setProperty name="ciudadano" property="fechaNacimiento" param="fechaNacimiento"/>
        <jsp:setProperty name="ciudadano" property="lugarNacimiento" param="lugarNacimiento"/>
        <jsp:setProperty name="ciudadano" property="nivelInstruccion" param="nivelInstruccion"/>
        <jsp:setProperty name="ciudadano" property="direccion" param="direccion"/>
        <jsp:setProperty name="ciudadano" property="tipoVivienda" param="tipoVivienda"/>
        <jsp:setProperty name="ciudadano" property="email" param="email"/>
         <p>Nombre: ${ciudadano.nombre}<br>
             Apellidos: ${ciudadano.apellido1} ${ciudadano.apellido2}<br>
            DNI: ${ciudadano.dni}<br>
            Fecha de nacimiento: ${ciudadano.fechaNacimiento}<br>
            Lugar de nacimiento: ${ciudadano.lugarNacimiento}<br>
            Nivel de instrucción ${ciudadano.nivelInstruccion}<br>
            Dirección ${ciudadano.direccion}<br>
            Tipo de vivienda: ${ciudadano.tipoVivienda}<br>
            email ${ciudadano.email}<br>

           
    </center>
    </body>
</html>
