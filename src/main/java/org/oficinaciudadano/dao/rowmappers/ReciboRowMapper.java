/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oficinaciudadano.dao.rowmappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.oficinaciudadano.beans.ReciboBean;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author Miguel
 */
public class ReciboRowMapper implements RowMapper {

    @Override
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        ReciboBean recibo = new ReciboBean();
        recibo.setIdRecibos(rs.getInt("IDRECIBOS"));
        recibo.setTipoPago(rs.getString("TIPOPAGO"));
        recibo.setSituacionPago(rs.getString("situacionpago"));
        recibo.setFechasAprobacion(rs.getDate("FECHASAPROBACION"));
        recibo.setFechaCobro(rs.getString("FECHACOBRO"));
        recibo.setFechaLimitePago(rs.getDate("fechaLimitePago"));
        recibo.setCategoria(rs.getString("CATEGORIA"));
        recibo.setEstado(rs.getString("estado"));
        return recibo;
    }
}
