/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oficinaciudadano.dao.jdbc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.oficinaciudadano.beans.ReciboBean;
import org.oficinaciudadano.dao.ReciboDAO;
import org.oficinaciudadano.dao.rowmappers.ReciboRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Miguel
 */
@Repository
public class JdbcReciboDAO implements ReciboDAO {

    @Autowired
    DataSource dataSource;

    @Override
    public ReciboBean obtenerRecibo(int idRecibo) throws Exception {
        List<ReciboBean> recibos;
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "select * "
                + "from( "
                + "     select r.IDRECIBOS,'IVTM' CATEGORIA,i.estado, i.TIPOPAGO,r.SITUACIONPAGO,r.FECHASAPROBACION,i.FECHACOBRO,r.FECHALIMITEPAGO  "
                + "     from recibos r inner join  IVTM i on r.IVTM_IDIVTM=i.IDIVTM "
                + "     UNION ALL "
                + "     select r.IDRECIBOS,'IBI' CATEGORIA,i.estado, i.TIPOPAGO,r.SITUACIONPAGO,r.FECHASAPROBACION,i.FECHACOBRO,r.FECHALIMITEPAGO "
                + "     from recibos r inner join GIP.IBI i on r.IBI_IDIBI=i.IDIBI"
                + ") r "
                + "where r.IDRECIBOS=? ";


        recibos = jdbcTemplate.query(sql, new Object[]{idRecibo},
                new ReciboRowMapper());

        if (recibos == null || recibos.isEmpty()) {
            throw new Exception("Recibo no encontrado.");
        }

        return recibos.get(0);

    }

    @Override
    public void crearRecibo(ReciboBean recibo) {
        String sql = "INSERT INTO recibos "
                + "(IDRECIBOS) VALUES (?)";

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.update(
                sql,
                new Object[]{recibo.getIdRecibos()});
    }
    
    public List<ReciboBean> listarRecibos(String dni) throws Exception {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        List<ReciboBean> recibos = new ArrayList<ReciboBean>();
        String sql = "select * from recibos where Ciudadanos_DNI = ?";
        /*resultado = jdbcTemplate.queryForObject(
                sql, new Object[]{dni}, String.class);

        return resultado;*/
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql,dni);
        for (Map rs : rows) {
            ReciboBean recibo = new ReciboBean();
            recibo.setIdRecibos((Integer) rs.get("idRecibos"));
            recibo.setSituacionPago((String) rs.get("situacionPago"));
            recibo.setFechasAprobacion((Date) rs.get("fechasAprobacion"));
            recibo.setCobro((String) rs.get("cobro"));
            recibo.setFechaLimitePago((Date) rs.get("fechaLimitePago"));
            recibos.add(recibo);
        }
        return recibos;
    }
}
