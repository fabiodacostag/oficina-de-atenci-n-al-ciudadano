/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oficinaciudadano.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Miguel
 */
public class ReciboBean implements Serializable {

    private Integer idRecibos;
    private String tipoPago;
    private String situacionPago;
    private Date fechasAprobacion;
    private String fechaCobro;
    private Date fechaLimitePago;
    private String categoria;
    private String estado;
    private Integer importe;
    private String cobro;

    public ReciboBean() {
        importe = ThreadLocalRandom.current().nextInt(100, 1000);;
    }

    public ReciboBean(Integer idRecibos) {
        this.idRecibos = idRecibos;
    }

    public Integer getIdRecibos() {
        return idRecibos;
    }

    public void setIdRecibos(Integer idRecibos) {
        this.idRecibos = idRecibos;
    }

    public String getSituacionPago() {
        return situacionPago;
    }

    public void setSituacionPago(String situacionPago) {
        this.situacionPago = situacionPago;
    }

    public Date getFechasAprobacion() {
        return fechasAprobacion;
    }

    public void setFechasAprobacion(Date fechasAprobacion) {
        this.fechasAprobacion = fechasAprobacion;
    }

    public String getFechaCobro() {
        return fechaCobro;
    }

    public void setFechaCobro(String fechaCobro) {
        this.fechaCobro = fechaCobro;
    }

    public Date getFechaLimitePago() {
        return fechaLimitePago;
    }

    public void setFechaLimitePago(Date fechaLimitePago) {
        this.fechaLimitePago = fechaLimitePago;
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescripcionSituacionPago() {

        if (this.situacionPago.equalsIgnoreCase("V")) {
            return "Voluntaria";
        }
        return "Requerida";
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcionEstado() {
        String res = "";

        if ("E".equalsIgnoreCase(this.estado)) {
            res = "Pendiente";
        } else if ("P".equalsIgnoreCase(this.estado)) {
            res = "Pagado";
        } else if ("A".equalsIgnoreCase(this.estado)) {
            res = "Anulado";
        }

        return res;
    }

    public String getCobro() {
        return cobro;
    }

    public void setCobro(String cobro) {
        this.cobro = cobro;
    }

    public Integer getImporte() {
        return importe;
    }

    @Override
    public String toString() {
        return "ReciboBean{" + "idRecibos=" + idRecibos + ", tipoPago=" + tipoPago + ", situacionPago=" + situacionPago + ", fechasAprobacion=" + fechasAprobacion + ", fechaCobro=" + fechaCobro + ", fechaLimitePago=" + fechaLimitePago + ", categoria=" + categoria + ", estado=" + estado + '}';
    }
}
