/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.oficinaciudadano.controllers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.oficinaciudadano.beans.CiudadanoBean;
import org.oficinaciudadano.beans.DireccionBean;
import org.oficinaciudadano.dao.jdbc.JdbcUsuarioDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 *
 * @author german
 */
@Controller
@SessionAttributes("ciudadano")
public class ControladorEmpleado {
    
    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    JdbcUsuarioDAO dao;
    
    @RequestMapping(value = "/insertarEnPadron",method = RequestMethod.POST)
    public String registroPadron(ModelMap model,@ModelAttribute("SpringWeb") CiudadanoBean ciudadano) {
        logger.info("Controlador Empleado > registrando en Padron " + ciudadano);
        logger.info("Controlador Empleado > mirando:  " + ciudadano.getNombre());

        boolean resultado;
        try{
            resultado=dao.insertarCiudadanoPadron(ciudadano);
             logger.info("Insertado");
        }
        catch(Exception e){
             logger.info("Excepcion: "+e);
        }

            return "registroPadronVerificar";       
    }  
}
