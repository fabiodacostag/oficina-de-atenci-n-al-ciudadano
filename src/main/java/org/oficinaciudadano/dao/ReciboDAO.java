/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oficinaciudadano.dao;

import java.util.List;
import org.oficinaciudadano.beans.ReciboBean;

/**
 *
 * @author Miguel
 */
public interface ReciboDAO {

    public ReciboBean obtenerRecibo(int idRecibo) throws Exception;

    public void crearRecibo(ReciboBean recibo);
    
    public List<ReciboBean> listarRecibos(String dni) throws Exception;
}
