<%-- 
    Document   : index
    Created on : 10-oct-2014, 18:22:55
    Author     : adrian
--%>

<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" type="image/x-icon" href="oc-img/warning-icon.png" />
        <link rel="stylesheet" type="text/css" href="oc-css/layout.css" />
        <script type="text/javascript" src="menu-desplegable.js"></script>
        <title>404 Yes Found</title>
    </head>

    <body>       
    <center>
        <%@ include file = "menu-desplegable.jsp" %> 
        <br> <br> <br> 
        <h1>Esta es la p�gina de inicio, saludos amigos</h1>
        <img src="oc-img/en-construccion.gif" alt="P�gina en construcci�n" width="500" height="500">
        <h2>Reg�strate ahora o inicia sesi�n</h2>
        <form action="registro-ciudadano.jsp" method="post" >
            <input type="submit" value="Registrarse"/>
        </form>
        <br>
      
        <form:form method="POST" action="/OficinaCiudadano/logging"  >
            <b><form:label path="usuario.nombre">Nombre de Usuario:</form:label></b>
            <form:input path="usuario.nombre" />
            <b><form:label path="password">Contrase�a:</form:label></b>
            <form:password path="password" />

            <input type="submit" name="login" value="Iniciar sesi�n" /> 
        </form:form>
            
    </center>
</body>
</html>
