/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oficinaciudadano.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.oficinaciudadano.dao.jdbc.JdbcUsuarioDAO;

/**
 *
 * @author adrian
 */
public class ListaSolicitudesBean implements Serializable{
 
    private List<CambioDatosBean> listaSolicitudesCambioDomicilio;

    public ListaSolicitudesBean(){
        JdbcUsuarioDAO dao = new JdbcUsuarioDAO();
        listaSolicitudesCambioDomicilio = dao.obtenerSolicitudesCambioDomicilio();
    }
    
    public List<CambioDatosBean> getListaSolicitudesCambioDomicilio() {
        return listaSolicitudesCambioDomicilio;
    }

    public void setListaSolicitudesCambioDomicilio(ArrayList<CambioDatosBean> listaSolicitudesCambioDomicilio) {
        this.listaSolicitudesCambioDomicilio = listaSolicitudesCambioDomicilio;
    }
}
