/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oficinaciudadano.beans;

import java.io.Serializable;

/**
 *
 * @author adrian
 */
public class DireccionBean implements Serializable{
    
    private String idDireccion;
    private String nombreCalle;
    private String numeroPortal;
    private String codigoPostal;

    public DireccionBean() {
    }

    public String getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(String idDireccion) {
        this.idDireccion = idDireccion;
    }

    public String getNombreCalle() {
        return nombreCalle;
    }

    public void setNombreCalle(String nombreCalle) {
        this.nombreCalle = nombreCalle;
    }

    public String getNumeroPortal() {
        return numeroPortal;
    }

    public void setNumeroPortal(String numeroPortal) {
        this.numeroPortal = numeroPortal;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }
}
