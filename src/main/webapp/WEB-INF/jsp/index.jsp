<%-- 
    Document   : index
    Created on : 10-oct-2014, 18:22:55
    Author     : adrian
--%>

<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" type="image/x-icon" href="oc-img/warning-icon.png" />
        <link rel="stylesheet" type="text/css" href="oc-css/layout.css" />
        <script type="text/javascript" src="menu-desplegable.js"></script>
        <title>404 Yes Found</title>
    </head>
    
    <body>       
    <center>
        <%@ include file = "menu-desplegable.jsp" %> 
        <br> <br> <br> 
        <h1>Esta es la p�gina de inicio, saludos amigos</h1>
        <h2 style="background-color: red">${mensaje}</h2>
        <h2>Reg�strate ahora o inicia sesi�n</h2>
        <form action="registro-ciudadano.htm" method="post" >
            <input type="submit" value="Registrarse"/>
        </form>
        <br>
        
        <c:choose>
            <c:when test="${sessionScope.usuario==null}">
                <form:form method="POST" action="logging.htm"  commandName="usuario" modelAttribute="usuario">
                    <b><form:label path="nombre">Nombre de Usuario:</form:label></b>
                    <form:input path="nombre" />
                    <b><form:label path="password">Contrase�a:</form:label></b>
                    <form:password path="password" />
                    
                    <input type="submit" name="login" value="Iniciar sesi�n" /> 
                </form:form>
            </c:when>
            
            <c:otherwise>
                <b>Bienvenido ${sessionScope.usuario.nombre}!</b>
                <a href="logout.htm"> Cerrar session</a>
                <p><a href="detalleRecibo.htm?id=1">Ejemplo detalle recibo</a></p>
            </c:otherwise>
        </c:choose>
    </center>
</body>
</html>
